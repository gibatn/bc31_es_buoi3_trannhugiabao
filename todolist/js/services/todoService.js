export let todoService = {
  layDSToDo: () => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/todo/",
      method: "GET",
    });
  },
  themMoiToDo: (toDo) => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/todo/",
      method: "POST",
      data: toDo,
    });
  },
  thayDoiStatus: (toDo) => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/todo/" + toDo.id,
      method: "PUT",
      data: toDo,
    });
  },
  xoaToDo: (id) => {
    return axios({
      url: "https://62b3cf60a36f3a973d271421.mockapi.io/todo/" + id,
      method: "DELETE",
    });
  },
};
