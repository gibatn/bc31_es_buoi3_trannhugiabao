export const spinnerService = {
  batLoading: () => {
    document.getElementById("loading").style.display = "flex";

    document.getElementById("card").style.display = "none";
  },

  tatLoading: () => {
    document.getElementById("loading").style.display = "none";

    document.getElementById("card").style.display = "block";
  },
};
