import { todoController } from "./Controller/todoController.js";
import { spinnerService } from "./services/spinnerToDo.js";
import { todoService } from "./services/todoService.js";
let todoArr = [];
let renderHTML = (list) => {
  document.getElementById("todo").style.display = "block";
  let contentHTMLDo = "";
  let contentHTMDont = "";
  for (let index in list) {
    let dsToDo = list[index];
    if (dsToDo.status == false) {
      contentHTMDont += `<li>${dsToDo.name} <div>
      
      <i class="fa fa-trash-alt " onclick="xoaToDo('${dsToDo.id}')"></i>
      <i class="fa fa-check-circle" onclick="changeStatus('${dsToDo.id}')"></i>
      </div>
      </li>`;
      document.getElementById("todo").innerHTML = contentHTMDont;
    }
    if (dsToDo.status == true) {
      contentHTMLDo += `<li>${dsToDo.name} 
      <div>
      <i class="fa fa-trash-alt" onclick="xoaToDo('${dsToDo.id}')"></i>
      <i class="fa fa-check-circle text-success"></i>
      </div>
      </li>`;
      document.getElementById("completed").innerHTML = contentHTMLDo;
    }
  }
};
let renderService = () => {
  spinnerService.batLoading();
  todoService
    .layDSToDo()
    .then((res) => {
      todoArr = res.data;

      renderHTML(todoArr);
      spinnerService.tatLoading();
    })
    .catch((err) => {});
};
renderService();

let themMoiToDo = () => {
  spinnerService.batLoading();

  let nameToDo = todoController.layThongTinTuForm();
  console.log(nameToDo);
  if (nameToDo.name !== "") {
    document.getElementById("tbNewTask").innerHTML = "";

    todoService
      .themMoiToDo(nameToDo)
      .then((result) => {
        renderService();
        spinnerService.tatLoading();
      })
      .catch((error) => {});
  } else {
    document.getElementById("tbNewTask").innerHTML = "Vui lòng nhập thông tin";
    document.getElementById("tbNewTask").style.color = "#FA396B";
    spinnerService.tatLoading();
  }
};
let addItem = () => {
  themMoiToDo();
  document.getElementById("newTask").value = "";
};
window.addItem = addItem;

let capNhatStatus = (idtodo) => {
  spinnerService.batLoading();

  for (let i = 0; i < todoArr.length; i++) {
    if (todoArr[i].id == idtodo) {
      todoArr[i].status = true;
      todoService
        .thayDoiStatus(todoArr[i])
        .then((res) => {
          renderService();
          spinnerService.tatLoading();
        })
        .catch((error) => {});
    }
  }
};
window.changeStatus = capNhatStatus;

let xoaToDo = (id) => {
  spinnerService.batLoading();

  todoService
    .xoaToDo(id)
    .then((res) => {
      renderService();
    })
    .catch((err) => {});
};

window.xoaToDo = xoaToDo;

let sortDown = () => {
  function compare(a, b) {
    // Dùng toUpperCase() để không phân biệt ký tự hoa thường
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();

    let comparison = 0;
    if (nameA > nameB) {
      comparison = 1;
    } else if (nameA < nameB) {
      comparison = -1;
    }
    return comparison * 1;
  }

  todoArr.sort(compare);
  renderHTML(todoArr);
};
window.sortDown = sortDown;
let sortUp = () => {
  function compare(a, b) {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();

    let comparison = 0;
    if (nameA < nameB) {
      comparison = 1;
    } else if (nameA > nameB) {
      comparison = -1;
    }
    return comparison * 1;
  }

  todoArr.sort(compare);

  renderHTML(todoArr);
};
window.sortUp = sortUp;
let todoDone = () => {
  document.getElementById("todo").style.display = "none";
};
window.todoDone = todoDone;

let sortTime = () => {
  function compare(a, b) {
    const nameA = new Date(a.time);
    const nameB = new Date(b.time);

    let comparison = 0;
    if (nameA.getTime() < nameB.getTime()) {
      comparison = 1;
    } else if (nameA.getTime() > nameB.getTime()) {
      comparison = -1;
    }
    return comparison * 1;
  }

  todoArr.sort(compare);

  renderHTML(todoArr);
};
window.sortTime = sortTime;
